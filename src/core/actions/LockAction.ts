import player from "../player";
import ISpaceAction from "../ISpaceAction";

export default class LockAction implements ISpaceAction {
    apply(player: player): void {
        player.lock();
    }
}