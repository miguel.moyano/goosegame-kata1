import player from "../player";
import ISpaceAction from "../ISpaceAction";

export default class ForwardAction implements ISpaceAction {
    private steps:number;

    constructor(steps:number) {
        this.steps = steps;
    }
    apply(player: player): void {
        player.moveForward(this.steps);
    }
}