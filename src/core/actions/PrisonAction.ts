import player from "../player";
import ISpaceAction from "../ISpaceAction";

export default class PrisonAction implements ISpaceAction {
    apply(player: player): void {
        player.moveTo(53);
        player.loseTurns(2);
    }
}