import Player from "./player";
import ISpaceAction from "./ISpaceAction";

export default class Space {

    protected number:number;
    protected description:string;
    protected actions:Array<ISpaceAction>;

    constructor(number:number, description:string){
        this.number = number;
        this.description = description;
        this.actions = new Array();
    } 

    addAction(action:ISpaceAction) {
        this.actions.push(action);
    }

    public getDescription() : string {
        return this.description;
    };

    public getNumber() : number {
        return this.number;
    }

    public applyActions (player:Player) : void {
        this.actions.forEach(action => {
            action.apply(player);
        });
    };

}

