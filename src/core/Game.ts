import Board from "./board";
import Dice from "./Dice";
import GameState from "./GameState";
import Player from "./player";
import Space from "./Space";

export default class Game {
    private board!: Board;
    private players!:Array<Player>;
    private currentPlayer!:number;
    private state!:GameState;
    
    constructor() {
        this.players = new Array();
        this.board = new Board();
        this.currentPlayer = 0;
    }

    public getBoard() {
        return this.board;
    }

    public getBoardSpaces() : Array<Space> {
        const spaces = [];
        for (let i = 1; i <= this.board.getSize(); i++) {
            spaces.push(this.board.getSpace(i));
        }
        return spaces;
    }

    public getPlayersFromSpace(space:number) {
        return this.players.filter(p => p.getPosition() === space);
    }

    public addPlayer(name:string, color:string) {
        this.players.push(new Player(name));
    }

    public getPlayer(name:string) {
        return this.players.find(p => p.getName() === name);
    }

    public getPlayers() {
        return this.players;
    }

    public getCurrentPlayer() {
        return this.players[this.currentPlayer];
    }

    public playNextTurn() : GameState {
        const player = this.getCurrentPlayer();
        const dice = this.getDiceResult();

        player.moveForward(dice); // return current position

        const space = this.board.getSpace(player.getPosition()); // return space info

        this.releaseLockedPlayers(); // return players state

        space.applyActions(player);
        
        this.state = {
            players: this.players,
            space: space,
            dice: dice
        }

        this.changeTurn();

        return this.state;
    }

    public getState() {
        return this.state;
    }

    public getWinner() {
        return this.players.find(p => p.isWinner());
    }

    private getDiceResult() {
        return Dice.rollDice(6);
    }

   

    private releaseLockedPlayers() {
        this.players.forEach(player => {
            if (player.getPosition() === this.getCurrentPlayer().getPosition()) {
                if (player.isLocked())
                    player.release();
            }
        });
     }

    private changeTurn() {
        this.currentPlayer++;
        if (this.currentPlayer >= this.players.length){
            this.currentPlayer = 0;
        }
        if (this.getCurrentPlayer().isLocked() || this.getCurrentPlayer().isWithoutTurn()) {
            this.getCurrentPlayer().decreaseLostTurns();
            this.changeTurn();
        }
    }

}