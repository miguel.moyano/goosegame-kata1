import Player from "./player";

export default interface ISpaceAction {
    apply(player:Player):void
}