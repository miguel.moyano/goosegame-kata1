import Space from "./Space";
import SpaceFactory from "./SpaceFactory";

export default class Board {
    private boardSize:number = 63;
    private spaces:Array<Space>;

    constructor(){
        this.spaces = new Array<Space>();
        this.createSpaces();
    } 
    
    public getSpace(position:number) : Space{
        if (position > this.boardSize) 
            return SpaceFactory.createBeyondSpace();

        return this.spaces[position-1];
    }

    public getSize() {
        return this.boardSize;
    }
    
    private createSpaces() {
        for (let i = 1; i<=this.boardSize; i++) {
            if (i == 6) {
                this.pushBridgeSpace();
            } else if (i == 19) {
                this.pushHotelSpace();
            } else if (i == 31) {
                this.pushWellSpace();
            } else if (i == 42) {
                this.pushMazeSpace();
            } else if (i >= 50 && i <= 55) {
                this.pushPrisonSpace();
            } else if (i == 58) {
                this.pushDeathSpace();
             } else if (i == 63) {
                 this.pushFinishSpace();
            } else if (i%6 == 0) {
                this.pushForwardSpace();
            } else {
                this.pushNormalSpace();
            }
        }
    }
    
    private pushNormalSpace() {
        this.spaces.push(SpaceFactory.createNormalSpace(this.spaces.length+1));   
    }
    
    private pushBridgeSpace() {
        this.spaces.push(SpaceFactory.createBridgeSpace(this.spaces.length+1, 12));
    }
    
    private pushForwardSpace() {
        this.spaces.push(SpaceFactory.createTwoStepsForwardSpace(this.spaces.length+1));
    }
    
    private pushFinishSpace() {
        this.spaces.push(SpaceFactory.createFinishSpace(this.spaces.length+1));
    }

    private pushHotelSpace() {
        this.spaces.push(SpaceFactory.createHotelSpace(this.spaces.length+1));
    }
 
    private pushWellSpace() {
        this.spaces.push(SpaceFactory.createWellSpace(this.spaces.length+1));
    }
 
    private pushMazeSpace() {
        this.spaces.push(SpaceFactory.createMazeSpace(this.spaces.length+1));
    }
   
    private pushDeathSpace() {
        this.spaces.push(SpaceFactory.createDeathSpace(this.spaces.length+1));
    }

    private pushPrisonSpace() {
        this.spaces.push(SpaceFactory.createPrisonSpace(this.spaces.length+1));
    }



}