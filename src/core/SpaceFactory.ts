import BridgeAction from "./actions/BridgeAction";
import ForwardAction from "./actions/ForwardAction";
import LockAction from "./actions/LockAction";
import LostTurnAction from "./actions/LostTurnAction";
import PrisonAction from "./actions/PrisonAction";
import WinAction from "./actions/WinAction";

import Space from "./Space";

export default class SpaceFactory {
    public static createNormalSpace(number:number) : Space {
        return new Space(number, `Stay in space ${number}`)  
    } 
    
    public static createBridgeSpace(number:number, target:number) : Space {
        const space = new Space(number, `The Bridge: Go to space ${target}`)
        space.addAction(new BridgeAction(target));
        return space;
    }
    
    public static createHotelSpace(number:number) : Space {
        const space = new Space(number,`The Hotel: Stay for (miss) one turn`);
        space.addAction(new LostTurnAction(1));
        return space;
    }

    public static createWellSpace(number:number) : Space {
        const space = new Space(number,`The Well: Wait until someone comes to pull you out - they then take your place`);
        space.addAction(new LockAction());
        space.addAction(new LostTurnAction(3));
        return space;
    }

    public static createMazeSpace(number: number): Space {
        const space = new Space(number,`The Maze: Go back to space 39`);
        space.addAction(new BridgeAction(39));
        return space;
    }
    
    public static createTwoStepsForwardSpace(number:number) : Space {
        const space = new Space(number, "Move two spaces forward");
        space.addAction(new ForwardAction(2));
        return space;
    }

    public static createFinishSpace(number:number) : Space {
        const space = new Space(number, "Finish: you ended the game");
        space.addAction(new WinAction())
        return space;
    }

    public static createBeyondSpace() : Space {
        const space = new Space(0, "Move to space 53 and stay in prison for two turns");
        space.addAction(new PrisonAction());
        return space;
    }

    public static createDeathSpace(number:number) : Space {
        const space = new Space(number, "Death: Return your piece to the beginning - start the game again");
        space.addAction(new BridgeAction(0));
        return space;
    }

    public static createPrisonSpace(number:number) : Space {
        const space = new Space(number, "The Prison: Wait until someone comes to release you - they then take your place");
        space.addAction(new LockAction());
        space.addAction(new LostTurnAction(3));
        return space;
    }


}