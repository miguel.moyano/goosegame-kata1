import Game from "../core/Game";
import UserConsole from "./UserConsole";

export default class MainScene {
    private game:Game;
    private playersEmojis:Array<string> = ["🐵", "🐸", "🐱", "🐶", "🐼", "🐷", "🦊", "🐰"];

    constructor() {
        this.game = new Game();
        this.initialize();
    }
    
    private async initialize() {
        this.showIntroduction();
        await this.askBeginGame();
        await this.askPlayers();
        await this.createGameLoop();
    }
    
    private async createGameLoop() {
        while(!this.game.getWinner()) {
            this.clearScreen();
            this.printBoard();
            await this.playPlayerTurn();
        }
        this.showGameOverMessage();
    }

    private showIntroduction() {
        this.printGooseHeader();
        UserConsole.out("Welcome to the famous and glorious Goose Game!! \n");
    }

    private async askBeginGame() {
        await UserConsole.in("Do you want to play? 🦢 ");
    }

    private async askPlayers() {
        const players = await UserConsole.in("How many players will be playing? ");
        for(let i=1; i<=parseInt(players); i++) {
            const playerName = await UserConsole.in(`${this.playersEmojis[i-1]} Name of player ${i}: `);
            this.game.addPlayer(playerName,"");
        }
    }

    private async playPlayerTurn() {
        await this.askPlayerRollDice();

        const state = this.game.playNextTurn();
        await UserConsole.in(`You got a ${state.dice}, lets see your position:`);
        await UserConsole.in(state.space.getDescription());

    }

    private async askPlayerRollDice() {
        const currentPlayer = this.game.getCurrentPlayer();
        const playerEmoji = this.playersEmojis[this.game.getPlayers().indexOf(currentPlayer)];
        const playerName = currentPlayer.getName();
        await UserConsole.in(`${playerEmoji} ${playerName}'s turn! roll the dice `);
    }

    private showGameOverMessage() {
        UserConsole.out(`${this.game.getCurrentPlayer().getName()} won the game!! `);
    }

    private clearScreen() {
        UserConsole.clear();
    }

    private printBoard() {
        let boardString = "The board:\n\n";
        const spaces = this.game.getBoardSpaces();
        spaces.forEach(space => {
            boardString += this.getSpacePrint(space.getNumber());
            if (space.getNumber()%6 === 0) boardString += "\n";
        }); 
        boardString += "\n";
        UserConsole.out(boardString);
    }

    private getSpacePrint(position: number) {
        const playersIcons = this.getPlayersIconPrint(position);
        return `[${position} ${playersIcons}] `;
    }

    private getPlayersIconPrint(position:number) {
        let playersString = "";
        this.game.getPlayers().forEach((player,i) => {
            if (player.getPosition() === position)
                playersString += this.playersEmojis[i];
            else {
                playersString += " ";
            }
        });
        return playersString;
    }

    private printGooseHeader() {
        UserConsole.out(`
                                   ___
                               ,-""   ..
                             ,'  _   e ).-._
                            /  ,' .-._<.===-'
                           /  /
                          /  ;
              _          /   ;
 (.._    _.-"" ""--..__,'    |
 <_  .-""                     \
  <.-                          :
   (__   <__.                  ;
     .-.   '-.__.      _.'    /
        \      .-.__,-'    _,'
         .._    ,    /__,-'
            ""._\__,'< <____
                 | |  .----...
                 | |        \ ..
                 ; |___      \-..
                 \   --<
                  ....<
                 .-'
        `);
    }

    
}