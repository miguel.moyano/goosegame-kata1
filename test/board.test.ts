import Board from "../src/core/Board";
import Player from "../src/core/player";
import Space from "../src/core/Space";

const getNewBoard = () : Board => {
    return new Board();
}

const getHotelSpace = () : Space => {
    return getNewBoard().getSpace(19);
}

const getWellSpace = () : Space => {
    return getNewBoard().getSpace(31);
}

const getMazeSpace = () : Space => {
    return getNewBoard().getSpace(42);
}

const getDeathSpace = () : Space => {
    return getNewBoard().getSpace(58);
}

const getFinishSpace = () : Space => {
    return getNewBoard().getSpace(63);
}

const getBeyondSpace = () : Space => {
    return getNewBoard().getSpace(64);
}

const getForwardSpace = () : Space => {
    return getNewBoard().getSpace(12);
}

const getTestPlayer = () : Player => {
    const player = new Player("test");
    player.moveTo(1);
    return player;
}

const getPrisonSpace = () : Space => {
    return getNewBoard().getSpace(50);
}

test("when get space multiply of six, and there is not another special space, show Forward Space description", () => {
    const space = getForwardSpace();
    expect(space.getDescription()).toBe("Move two spaces forward");
})

test("when player get in Forward Space, it must advance two spaces", () => {
    const player = getTestPlayer();
    player.moveTo(12);
    const space = getForwardSpace();
    space.applyActions(player);
    expect(player.getPosition()).toBe(14);
});

test("when get space 19 description, return The Hotel Space description", () => {
    const space = getHotelSpace();
    expect(space.getDescription()).toBe("The Hotel: Stay for (miss) one turn");
})

test("when player get in Hotel Space, it must miss a turn", () => {
    const player = getTestPlayer();
    const space = getHotelSpace();
    space.applyActions(player);
    expect(player.isWithoutTurn()).toBe(true);
    player.decreaseLostTurns();
    expect(player.isWithoutTurn()).toBe(false);
})

test("when get space 31 description, return The Well Space description", () => {
    const space = getWellSpace();
    expect(space.getDescription()).toBe("The Well: Wait until someone comes to pull you out - they then take your place");
})

test("when get space 42 description, return The Maze description", () => {
    const space = getMazeSpace();
    expect(space.getDescription()).toBe("The Maze: Go back to space 39");
})

test("when player get in Maze Space, it must go to space number 39", () => {
    const player = getTestPlayer();
    const space = getMazeSpace();
    space.applyActions(player);
    expect(player.getPosition()).toBe(39);
})

test("When get space 58, return Death space description", () => {
    const space = getDeathSpace();
    expect(space.getDescription()).toBe("Death: Return your piece to the beginning - start the game again");
})

test("When player get in Death Space, player must return to zero position", () => {
    const player = getTestPlayer();
    const space = getDeathSpace();
    space.applyActions(player);
    expect(player.getPosition()).toBe(0);
})

test("When get space 63, return Finish space description", () => {
    const space = getFinishSpace();
    expect(space.getDescription()).toBe("Finish: you ended the game");
})

test("When player get in Finish Space, player must be winner", () => {
    const player = getTestPlayer();
    const space = getFinishSpace();
    space.applyActions(player);
    expect(player.isWinner()).toBe(true);
})

test("When player get beyond space 63, show Beyond Space message", () => {
    const space = getBeyondSpace();
    expect(space.getDescription()).toBe("Move to space 53 and stay in prison for two turns");
})

test("When player get in Beyond Space, player must be on prison", () => {
    const player = getTestPlayer();
    const space = getBeyondSpace();
    space.applyActions(player);
    expect(player.getPosition()).toBe(53);
    expect(player.isWithoutTurn()).toBe(true);
    player.decreaseLostTurns();
    player.decreaseLostTurns();
    expect(player.isWithoutTurn()).toBe(false);
})

test("When player is between 50 and 55, print prison description", () => {
    const board = getNewBoard();
    for(let i=50; i<=55;i++) {
        const space = board.getSpace(i);
        expect(space.getDescription()).toBe("The Prison: Wait until someone comes to release you - they then take your place");
    }
})

test("When player get in prison, must be locked", () => {
    const space = getPrisonSpace();
    const player = getTestPlayer();
    space.applyActions(player);
    expect(player.isLocked()).toBe(true);
})
