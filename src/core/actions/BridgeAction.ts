import player from "../player";
import ISpaceAction from "../ISpaceAction";

export default class BridgeAction implements ISpaceAction{
    private target:number;
    
    constructor(target:number) {
        this.target = target;
    }

    apply(player: player): void {
        player.moveTo(this.target);
    }
}