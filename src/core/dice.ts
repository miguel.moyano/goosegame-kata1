export default class Dice {
    public static rollDice(diceSize:number) {
        return Math.floor(Math.random() * (diceSize) + 1)
    }
}