import player from "../player";
import ISpaceAction from "../ISpaceAction";

export default class WinAction implements ISpaceAction {
    apply(player: player): void {
        player.win();
    }
}