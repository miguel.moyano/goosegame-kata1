import player from "../player";
import ISpaceAction from "../ISpaceAction";

export default class LostTurnAction implements ISpaceAction {
    private turns:number;
    constructor(turns:number) {
        this.turns = turns;
    }
    apply(player: player): void {
        player.loseTurns(this.turns);
    }
}