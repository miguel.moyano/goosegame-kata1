import Player from "./player";
import Space from "./Space";

export default interface GameState {
    space:Space;
    players:Array<Player>;
    dice:number;
}