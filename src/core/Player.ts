import Dice from "./Dice";

export default class Player {
    private name:string;
    private position:number;
    private lostTurns:number;
    private winner:boolean;
    private locked:boolean;

    constructor(name:string) {
        this.name = name;
        this.position = 0;
        this.lostTurns = 0;
        this.winner = false;
        this.locked = false;
    }

    public getName() {
        return this.name;
    }

    public getPosition() {
        return this.position;
    }

    public moveForward(spaces:number) {
        this.position += spaces;
    }

    public moveBackward(spaces:number) {
        this.position -= spaces;
    }

    public moveTo(space:number) {
        this.position = space;
    }

    public loseTurns(turns:number) {
        this.lostTurns = turns;
    }

    public decreaseLostTurns() {
        this.lostTurns--;
        if (this.lostTurns <0) {
            this.lostTurns = 0;
            this.locked = false;
        }
    }

    public isWithoutTurn() {
        return this.lostTurns > 0;
    }

    public isWinner() {
        return this.winner;
    }

    public win() {
        this.winner = true;
    }

    public lock() {
        this.locked = true;
    }

    public release() {
        this.locked = false;
        this.lostTurns = 0;
    }

    public isLocked() {
        return this.locked;
    }


}