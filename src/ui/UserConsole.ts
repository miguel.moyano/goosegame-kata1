import ReadLine from "readline";

export default class UserConsole {

    public static in(question: string) {
        const reader = ReadLine.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        return new Promise<string>(resolve => reader.question(question, answer => {
            reader.close();
            resolve(answer);
        }))
    }

    public static out(message:string) {
        console.log(message);
    }

    public static clear() {
        console.clear();
    }
}